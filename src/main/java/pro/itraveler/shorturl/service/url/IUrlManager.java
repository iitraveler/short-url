package pro.itraveler.shorturl.service.url;

import java.util.Iterator;
import java.util.Map;

import pro.itraveler.shorturl.exception.ServiceException;

public interface IUrlManager {
	public String addUrl(String originUrl) throws ServiceException;
	public String getOriginUrl(String key);
	public void removeUrl(String key);
	public Iterator<Map.Entry<String, String>> getList();
}
