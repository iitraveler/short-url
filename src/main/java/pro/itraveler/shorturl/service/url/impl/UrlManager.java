package pro.itraveler.shorturl.service.url.impl;

import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pro.itraveler.shorturl.controller.validator.DataValidator;
import pro.itraveler.shorturl.exception.ServiceException;
import pro.itraveler.shorturl.lib.util.Base62;
import pro.itraveler.shorturl.service.url.IUrlManager;

@Service
public class UrlManager implements IUrlManager {

	@Autowired
	DataValidator validator;
	
	private Long seq = new Long(0);
	private ConcurrentHashMap<String, String> seqMap = new ConcurrentHashMap<>();
	private ConcurrentHashMap<String, String> urlMap = new ConcurrentHashMap<>();
	
	@Override
	public String addUrl(String originUrl) throws ServiceException {
		
		if (!validator.isValidUrl(originUrl))
			throw new ServiceException("data.invalid");
		
		// already exist key
		String oldKey = urlMap.get(originUrl);
		if (oldKey != null) {
			throw new ServiceException("data.exist.already");
			//return oldKey;
		}
		
		String oldUrl;
		String key;
		
		synchronized (seq) {
			key = Base62.encode(++seq); 
			oldUrl = seqMap.putIfAbsent(key, originUrl);
			
			if (oldUrl != null || key == null)
				throw new ServiceException("system.error.data");
			
			urlMap.putIfAbsent(originUrl, key);
		}
		
		return key;
	}

	@Override
	public void removeUrl(String key) {
		
		if (key == null || key.equals(""))
			return;
		
		String url = seqMap.remove(key);
		
		if (url != null)
			urlMap.remove(url);
	}

	@Override
	public String getOriginUrl(String key) {
		
		return seqMap.get(key);
	}

	@Override
	public Iterator<Entry<String, String>> getList() {
		
		return seqMap.entrySet().iterator();
	}
}