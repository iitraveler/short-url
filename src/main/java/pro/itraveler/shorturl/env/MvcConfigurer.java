package pro.itraveler.shorturl.env;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.web.servlet.ErrorPage;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import pro.itraveler.shorturl.config.AppConfig;
import pro.itraveler.shorturl.controller.interceptor.LogInterceptor;

@Configuration
public class MvcConfigurer extends WebMvcConfigurerAdapter {
	
	@Autowired
    private AppConfig appConfig;
	
	@Override
    public void addInterceptors(InterceptorRegistry registry) {
		// DO NOT CHANGE BELOW INTERCEPTORS' ORDER
		// for DEBUG
		if (appConfig.getEnvironment().equals("Dev")) {
			HandlerInterceptor logIntercepter = new LogInterceptor();
			registry.addInterceptor(logIntercepter);
		}
    }
	
	@Bean
    public MessageSource messageSource() {
        
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setCacheSeconds(600);
        messageSource.setBasenames(appConfig.getMessageResource());
        messageSource.setDefaultEncoding("UTF-8");
        
        return messageSource;
    }
	
	@Bean
	public LocaleResolver localeResolver() {
		
		SessionLocaleResolver sessionLocaleResolver = new SessionLocaleResolver();
		sessionLocaleResolver.setDefaultLocale(StringUtils.parseLocaleString("en"));
		
		return sessionLocaleResolver;
	}

/*	
	@Bean
    public ViewResolver getViewResolver() {
		
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/WEB-INF/jsp/");
        resolver.setSuffix(".jsp");
        resolver.setOrder(0);
        return resolver;
    }
*/

//	@Override
//  public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
//      configurer.enable();
//  }
	
//	@Bean
//	public FilterRegistrationBean filterRegistration() {
//	
//		FilterRegistrationBean registration = new FilterRegistrationBean();
//		registration.setFilter(httpFilter);
//		registration.addUrlPatterns("/static/*");
//		//registration.setOrder(0);
//		//registration.setName("httpFilter");
//		return registration;
//	}
	
	@Bean
	public EmbeddedServletContainerCustomizer containerCustomizer() {

		return new EmbeddedServletContainerCustomizer() {
			@Override
			public void customize(ConfigurableEmbeddedServletContainer container) {

//				ErrorPage error500Page = new ErrorPage(HttpStatus.INTERNAL_SERVER_ERROR, "/WEB-INF/jsp/error/500.jsp");
//				ErrorPage error400Page = new ErrorPage(HttpStatus.BAD_REQUEST, "/WEB-INF/jsp/error/400.jsp");
				ErrorPage error404Page = new ErrorPage(HttpStatus.NOT_FOUND, "/WEB-INF/jsp/error/404.jsp");
//				ErrorPage error405Page = new ErrorPage(HttpStatus.METHOD_NOT_ALLOWED, "/WEB-INF/jsp/error/405.jsp");
				
				//container.addErrorPages(error404Page, error500Page, error400Page, error405Page);
				container.addErrorPages(error404Page);
			}
		};
	}
}