package pro.itraveler.shorturl.controller.handler;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;

import pro.itraveler.shorturl.config.AppConfig;
import pro.itraveler.shorturl.exception.ServiceException;

@Order(Ordered.LOWEST_PRECEDENCE)
@ControllerAdvice(annotations = Controller.class)
public class CommonExceptionHandler {

	@Autowired
    private AppConfig appConfig;
	
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private LocaleResolver localeResolver;

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@ExceptionHandler(ServiceException.class)
	public ModelAndView serviceExceptionHandler(ServiceException e, HttpServletRequest request){
		
		String errorMessage = e.getErrorMessage(messageSource, localeResolver.resolveLocale(request));
		e.setErrorMessage(errorMessage);
		
		ModelAndView mnv = new ModelAndView();
		mnv.addObject("appConfig", appConfig);
		mnv.addObject("serviceException", e);
		mnv.addObject("lang", localeResolver.resolveLocale(request));
		mnv.setViewName("error/common");
		
		logger.error("[CommonExceptionHandler.serviceExceptionHandler] code:{}, msg:{}, addmsg:{}, debmsg:{}", e.getErrorCode(), errorMessage, e.getAddMessage(), e.getDebugMessage());
	
		return mnv;
	}
	
	@ExceptionHandler(Exception.class)
	public ModelAndView defaultExceptionHandler(Exception e, HttpServletRequest request){
		
		ServiceException pe;
		if (e instanceof SQLException){
			pe = new ServiceException("system.error.db");
		} else {
			pe = new ServiceException("system.error.unknown");
		}
		
		String errorMessage = pe.getErrorMessage(messageSource, localeResolver.resolveLocale(request));
		pe.setErrorMessage(errorMessage);
		
		ModelAndView mnv = new ModelAndView();
		mnv.addObject("appConfig", appConfig);
		mnv.addObject("sampleException", pe);
		mnv.addObject("lang", localeResolver.resolveLocale(request));
		mnv.setViewName("error/common");
		logger.error("[CommonExceptionHandler.defaultExceptionHandler] class:{}, msg:{}", e.getClass(), e.getMessage());
		
		return mnv;
	}
}