package pro.itraveler.shorturl.controller.handler;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.LocaleResolver;

import pro.itraveler.shorturl.exception.ServiceException;
import pro.itraveler.shorturl.types.JsonResult;


@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice(annotations = RestController.class)
public class RestExceptionHandler {

	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private LocaleResolver localeResolver;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@ResponseBody
	@ExceptionHandler(ServiceException.class)
	public JsonResult serviceExceptionHandler(ServiceException e, HttpServletRequest request){
		
		String errorMessage = e.getErrorMessage(messageSource, localeResolver.resolveLocale(request));
		logger.error("[RestExceptionHandler.serviceExceptionHandler] code:{}, msg:{}, addmsg:{}, debmsg:{}", e.getErrorCode(), errorMessage, e.getAddMessage(), e.getDebugMessage());
		
		return JsonResult.fail(-1, errorMessage);
	}
	
	@ResponseBody
	@ExceptionHandler(Exception.class)
	public JsonResult defaultExceptionHandler(Exception e, HttpServletRequest request){
		
		ServiceException sampleException;
		if (e instanceof SQLException){
			sampleException = new ServiceException("system.error.db");
		} else {
			sampleException = new ServiceException("system.error.unknown");
		}
		
		String errorMessage = sampleException.getErrorMessage(messageSource, localeResolver.resolveLocale(request));
		logger.error("[RestExceptionHandler.defaultExceptionHandler] class:{}, msg:{}", e.getClass(), e.getMessage());
		
		return JsonResult.fail(-2, errorMessage);
	}
}