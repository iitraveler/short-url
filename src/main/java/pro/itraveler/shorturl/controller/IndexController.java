package pro.itraveler.shorturl.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pro.itraveler.shorturl.service.url.IUrlManager;

@Controller
public class IndexController {

	@Autowired
    IUrlManager urlManager;
	
//	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(Model model, HttpServletRequest request) throws Exception {

		model.addAttribute("list", urlManager.getList());
		
		return "home/index";
	}
	
	@RequestMapping(value = "/{key}", method = RequestMethod.GET)
	public String redirectUrl(@PathVariable String key) throws Exception {

		String url = urlManager.getOriginUrl(key);
		
		if (url == null || url.equals(""))
			return "redirect:/";
		else
			return "redirect:" + url;
	}
	
}