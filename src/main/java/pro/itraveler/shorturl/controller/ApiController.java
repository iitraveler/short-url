package pro.itraveler.shorturl.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pro.itraveler.shorturl.service.url.IUrlManager;
import pro.itraveler.shorturl.types.JsonResult;

@RestController
public class ApiController {
	@Autowired
	private IUrlManager urlManager;

	// private Logger logger = LoggerFactory.getLogger(getClass());

	@RequestMapping(value = "/", method = { RequestMethod.POST })
	public JsonResult addUrl(@RequestParam(value = "url", defaultValue = "") String url) throws Exception {

		String key = urlManager.addUrl(url);
		JsonResult jsonResult;
		
		if (key == null || key.equals(""))
			jsonResult = JsonResult.fail();
		else
			jsonResult = JsonResult.success(key);

		return jsonResult;
	}
	
	@RequestMapping(value = "/{key}", method = { RequestMethod.DELETE })
	public JsonResult removeUrl(@PathVariable String key) throws Exception {

		urlManager.removeUrl(key);
		
		return JsonResult.success();
	}

}