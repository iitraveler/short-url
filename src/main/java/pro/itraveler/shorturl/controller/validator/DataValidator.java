package pro.itraveler.shorturl.controller.validator;

import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

@Service
public class DataValidator {

	 private static final String regex = "^(https?):\\/\\/([^:\\/\\s]+)(:([^\\/]*))?((\\/[^\\s/\\/]+)*)?\\/?([^#\\s\\?]*)(\\?([^#\\s]*))?(#(\\w*))?$";
     private static final Pattern p = Pattern.compile(regex);
    		 
	public boolean isValidUrl(String url) {
		return p.matcher(url).matches();
	}
}
