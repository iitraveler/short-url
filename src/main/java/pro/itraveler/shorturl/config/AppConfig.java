package pro.itraveler.shorturl.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "application")
public class AppConfig {

	private String environment;
	private String messageResource;

	/*
	@PostConstruct
	private void postConstruct() {
		
	}
	*/
	
	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	public String getMessageResource() {
		return messageResource;
	}

	public void setMessageResource(String messageResource) {
		this.messageResource = messageResource;
	}

}