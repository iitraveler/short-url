package pro.itraveler.shorturl.lib.util;

public class Base62 {
	
	private static final char base[] = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();

	public static String encode(long id)
	{   
	    int m;
	    StringBuffer buf = new StringBuffer();
	    
	    do {
	        m = (int) id % 62;
	        buf.append(base[m]); // Character.toString(base[m])
	        id = id / 62;
	    } while (id > 0);
	    
	    buf.reverse();
	    return buf.toString();
	}

	public static long decode(String code)
	{
		int size = code.length();
	    long result = 0;
	    long mul = 1;
	    
	    if (size == 0)
	    	return 0;
	    
	    char input[] = code.toCharArray();
	    
	    for(int i = size - 1; i >= 0; i--)
	    {
	    	for (int j = 0; j < 62; j++) {
	    		if (input[i] == base[j]) {
	    			result += j * mul;
	    		}
	    	}
	    	
	    	mul *= 62;
	    }
	    
	    return result;
	}
}