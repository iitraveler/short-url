<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" session="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ftt" tagdir="/WEB-INF/tags/"%>
<!doctype html>
<html lang="${lang}">
<head>
<title>ERROR</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div id="container">
    <h2>ERROR</h2>
    <p class="text">
      <span>MESSAGE: ${serviceException.getErrorMessage()} ${serviceException.getAddMessage() ? "("+serviceException.getAddMessage()+")" : ''}</span>
      <br/>
      <span>CODE: ${appConfig.getEnvironment().equals("Dev") ? serviceException.getErrorCode() : ""}</span>
    </p>
</div>
</body>
</html>