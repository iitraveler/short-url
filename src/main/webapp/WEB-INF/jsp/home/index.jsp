<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" session="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tpl" tagdir="/WEB-INF/tags/"%>
<tpl:commonLayout subtitle=" - Home">
<jsp:attribute name="header">
<script>
</script>
</jsp:attribute>
<jsp:attribute name="body">
	<div>ORIGIN URL : <input type="url" size=100 name="url"> <input type="button" id="btnAdd" value="Add"></div>
	<br/>
	
	<c:forEach var="item" items="${list}" varStatus="status">
    <a href="/${item.key}" target="_blank">http://localhost:18080/${item.key} >> ${item.value}</a> &nbsp;<input type="button" name="btnRemove" value="Remove ${item.key}"><br/>
	</c:forEach>
	
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>

<script type="text/javascript">
<!--
$( document ).ready(function() {
	$('#btnAdd').on('click', onClickAdd);
	$('input[name="url"]').keydown( function(key) {
	    if(key.keyCode == 13) {
	    	onClickAdd();
	    }
	});
	$('input[name="btnRemove"]').on('click', onClickRemove);
	
	$('input[name="url"]').focus();
});

function onClickAdd() {
	var strUrl = $('input[name="url"]').val();
	
	if (strUrl == '') {
		alert("URL 을 입력해 주세요!");
		$('input[name="url"]').focus();	
		return;
	}
	
	$.ajax({
		type: 'POST',
		url: '/',
		data: { 'url' : strUrl },
		success: function(ret, status, xhr){
			if (typeof(ret) == 'object') {
				if (ret.code == 0) { // success
					alert('추가 성공!');
					document.location.reload();
				}
		  		else {
		  			alert('추가 실패!' + (ret.message ? '(' + ret.message +')' : ''));
		  		}
			}
			else {
				console.log(ret);
			}
		},
		dataType: 'json'
	});
}

function onClickRemove(e) {
	var strVal = $(e.target).val();
	var strKey = strVal.substr(7);
	
	$.ajax({
		type: 'DELETE',
		url: '/'+strKey,
		success: function(ret, status, xhr){
			if (typeof(ret) == 'object') {
				if (ret.code == 0) { // success
					alert('삭제 성공!');
					document.location.reload();
				}
		  		else {
		  			alert('삭제 실패!' + (ret.message ? '(' + ret.message +')' : ''));
		  		}
			}
			else {
				console.log(ret);
			}
		},
		dataType: 'json'
	});
}
-->
</script>
</jsp:attribute>
</tpl:commonLayout>