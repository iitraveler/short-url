<!doctype html>
<%@ tag language="java" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="tpl" tagdir="/WEB-INF/tags/"%>

<%@attribute name="subtitle"%>
<%@attribute name="header" fragment="true" %>
<%@attribute name="body" fragment="true" %>
<html lang="ko">
<head>
<tpl:commonHeader />
<title>Short URL Service ${subtitle}</title>
<jsp:invoke fragment="header"/>
</head>
<body>
<jsp:invoke fragment="body"/>
<tpl:commonFooter />
</body>
</html>