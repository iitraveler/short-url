<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<s:eval expression="@environment.getProperty('project.domain.static')" var="staticDomain"/>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">